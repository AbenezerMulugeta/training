<?php
class NoticeBoard_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$this->load->view('noticeBoard_view');
	}

	public function post(){
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');
		if ($this->form_validation->run()==FALSE){
				$this->load->view('noticeBoard_view');
			}else{
				$data = array('title'         			=> set_value('title'),
							  'date'					=> date('Y-m-d'),
							  'message'           		=> set_value('message')
					);
				$this->Main_model->post($data);
				redirect('NoticeBoard_controller');
				
			}
	}
}