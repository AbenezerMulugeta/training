<?php
class Trainers_update_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->trainers_view_for_update();
			if($query){
				$data['trainers_update'] = $query;
			}
			$query1 = $this->Main_model->trainings_drop_down();
			if($query1){
				$data['trainings'] = $query1;
			}
		$this->load->view('trainers_update_view', $data);
	}

	public function update_trainers(){
		    $this->form_validation->set_rules('select_trainers', 'Trainer', 'required');
			if ($this->form_validation->run()==FALSE){
				$this->load->view('trainers_update_view');
			}else{
				if($_FILES['userfile']['name'] != '')
				{	   	$config['upload_path']          = './assets/uploads/';
						$config['allowed_types']        = 'jpg|jpeg|png|doc|txt|pdf|doc|docx|sql|zip|rar';
						$config['max_size']             = 100000;
						$config['max_width']            = 100000;
						$config['max_height']           = 100000;
						$this->load->library('upload', $config);
						
					if ( ! $this->upload->do_upload())
					{	$this->load->view('trainers_update_view');
			}else{
				$upload_image = $this->upload->data();
				$select = set_value('select_trainers');
				$update = array(
							  'age'       				=> set_value('age'),
							  'phone'           		=> set_value('phone'),
							  'city' 					=> set_value('city'),
							  'email'           		=> set_value('email'),
							  'training'           		=> set_value('training'),
							  'experience'           	=> set_value('experience'),
							  'specialized_field'       => set_value('specialized_field'),
							  'file_address1'			=> $upload_image['file_name'],
							  'salary'                	=> set_value('salary'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status'),
							  'view' 					=> set_value('0')
							  );

			$this->Main_model->update_trainers($update,$select);
			}
			
			redirect('Trainers_update_controller');
		}
	}
}
}