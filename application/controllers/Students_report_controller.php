<?php
class Students_report_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->model_trainees_view();
			$data['trainees'] = null;
			if($query){
				$data['trainees'] = $query;
			}
		$this->load->view('students_report_view', $data);
	}
}