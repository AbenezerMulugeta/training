<?php
class New_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		    $query = $this->Main_model->model_trainings_view_status();
			$data['trainings'] = null;
			if($query){
				$data['trainings'] = $query;
			}

			$query1 = $this->Main_model->model_trainers_view_status();
			$data['trainers'] = null;
			if($query1){
				$data['trainers'] = $query1;
			}

			$query2 = $this->Main_model->model_trainees_view_status();
			$data['trainees'] = null;
			if($query2){
				$data['trainees'] = $query2;
			}
		$this->load->view('new_view', $data);
	}

	public function update_trainings(){
		$course = set_value('course');
		$update = array('view'		=>		'1');
		$this->Main_model->update_trainings_view($update, $course);
		redirect('New_controller');
	}

	public function update_trainers(){
		$trainers = set_value('trainers');
		$update = array('view'		=>		'1');
		$this->Main_model->update_trainers_view($update, $trainers);
		redirect('New_controller');
	}

	public function update_students(){
		$students = set_value('students');
		$update = array('view'		=>		'1');
		$this->Main_model->update_students_view($update, $students);
		redirect('New_controller');
	}
}