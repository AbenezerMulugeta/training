<?php
class Students_off_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$update = array('status'		=>		'contract_completed');
		$this->Main_model->update_students_status($update);
		$query = $this->Main_model->students_off();
			$data['trainees'] = null;
			if($query){
				$data['trainees'] = $query;
			}
		$this->load->view('students_off_view', $data);
	}
}