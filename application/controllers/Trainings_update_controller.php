<?php
class Trainings_update_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->trainings_view_for_update();
			if($query){
				$data['trainings_update'] = $query;
			}
		$this->load->view('trainings_update_view', $data);
	}

	public function update_trainings(){
		$select = set_value('select_training');
			$update = array('specify_schedule' 			=> set_value('period'),
							'fee' 						=> set_value('fee'),
							'program'					=> set_value('program'),
							'view' 						=> set_value('0'));


			$this->Main_model->update_trainings($update,$select);
			redirect('Trainings_update_controller');
	}
}