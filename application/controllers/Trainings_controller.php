<?php
class Trainings_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$this->load->view('trainings_view');
	}

	public function register_trainings(){
			$this->form_validation->set_rules('training', 'Training', 'required');
			$this->form_validation->set_rules('fee', 'Fee', 'required');
			if ($this->form_validation->run()==FALSE){
				$this->load->view('trainings_view');
			}else{
				$data = array('training_id'  				=> set_value('trainingID'),
						      'training'         			=> set_value('training'),
						      'course_description'         	=> set_value('course_description'),
							  'vendor' 	   					=> set_value('vendor'),
							  'specify_schedule'			=> set_value('specify_schedule'),
							  'program'						=> set_value('program'),
							  'fee'       					=> set_value('fee')
					);
				$this->Main_model->training_register($data);
				redirect('Trainings_controller');
			}
		}
}