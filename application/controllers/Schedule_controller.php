<?php
class Schedule_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->model_trainings_view();
			$data['trainings'] = null;
			if($query){
				$data['trainings'] = $query;
			}
		$this->load->view('schedule_view', $data);
	}

	public function create_schedule(){
		$this->form_validation->set_rules('course', 'Course', 'required');
		if ($this->form_validation->run()==FALSE){
				$this->load->view('schedule_view');
			}else{
				$data = array('created_date'         		=> date('Y-m-d'),
							  'course'						=> set_value('course'),
							  'Monday_start'           		=> set_value('course1_start'),
							  'Tuesday_start'           	=> set_value('course2_start'),
							  'Wednesday_start'           	=> set_value('course3_start'),
							  'Thursday_start'           	=> set_value('course4_start'),
							  'Friday_start'           		=> set_value('course5_start'),
							  'Saturday_start'           	=> set_value('course6_start'),
							  'Sunday_start'           		=> set_value('course7_start'),
							  'Monday_end'           		=> set_value('course1_end'),
							  'Tuesday_end'           		=> set_value('course2_end'),
							  'Wednesday_end'           	=> set_value('course3_end'),
							  'Thursday_end'           		=> set_value('course4_end'),
							  'Friday_end'           		=> set_value('course5_end'),
							  'Saturday_end'           		=> set_value('course6'),
							  'Sunday_end'           		=> set_value('course7_end'),
					);
				$this->Main_model->create_schedule($data);
				redirect('Schedule_controller');
				
			}
	}
}
