<?php
class Control_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->view_post();
			$data['post'] = null;
			if($query){
				$data['post'] = $query;
			}
		$this->load->view('control_view', $data);
	}
}