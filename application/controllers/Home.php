<?php
class Home extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		if ($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			$data['id'] = $session_data['id'];
			$data['fullname'] = $session_data['fullname'];
			$data['username'] = $session_data['username'];
			$this->load->view('home_view', $data);
		}else{
			redirect('login_controller', 'refresh');
		}
	}

	function logout(){
		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		redirect(site_url('login_controller'), 'refresh');
	}
}