<?php
class Students_update_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->trainees_view_for_update();
			if($query){
				$data['trainees_update'] = $query;
			}
			$query2 = $this->Main_model->trainings_drop_down();
			if($query2){
				$data['trainings'] = $query2;
			}

			$query1 = $this->Main_model->trainers_drop_down();
			if($query1){
				$data['trainer'] = $query1;
			}
		$this->load->view('students_update_view', $data);
	}

	public function update_students(){
			$select = set_value('select_trainees');
			$update = array(  'phone'           		=> set_value('phone'),
							  'email' 					=> set_value('email'),
							  'city'           			=> set_value('city'),
							  'company'           		=> set_value('company'),
							  'class'           		=> set_value('class'),
							  'program'           		=> set_value('program'),
							  'education_level' 		=> set_value('education_level'),
							  'schedule_fee'           	=> set_value('schedule_fee'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status'),
							  'view' 					=> set_value('0')
							);

			$this->Main_model->update_trainees($update,$select);
			redirect('Students_update_controller');
		}
}