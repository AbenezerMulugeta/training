<?php
class Schedule_view_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->select_schedule();
			$data['schedule'] = null;
			if($query){
				$data['schedule'] = $query;
			}
		$this->load->view('schedule_view_view', $data);
	}

	public function remove_schedule(){
		$id = set_value('course');
		$this->Main_model->remove_schedule($id);
		redirect('Schedule_view_controller');
	}
}