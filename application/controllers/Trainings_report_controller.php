<?php
class Trainings_report_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->model_trainings_view();
			$data['trainings'] = null;
			if($query){
				$data['trainings'] = $query;
			}
		$this->load->view('trainings_report_view', $data);
	}
}