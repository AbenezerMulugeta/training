<?php
class Account_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$this->load->view('account_view');
	}

	public function create_account(){
			$this->form_validation->set_rules('fname', 'Fullname', 'required');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('role', 'Role', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if ($this->form_validation->run()==FALSE){
				$this->load->view('account_view');
			}else{
				$data = array('fullname'         		=> set_value('fname'),
							  'username'         		=> set_value('username'),
							  'role' 	   				=> set_value('role'),
							  'password' 	   			=> md5(set_value('password')),
							  'status'       			=> '1'
							  
					);
				$this->Main_model->create_account($data);
				redirect('Account_controller');
				
			}
	}

			
}