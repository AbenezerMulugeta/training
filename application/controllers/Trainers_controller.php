<?php
class Trainers_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->trainings_drop_down();
			if($query){
				$data['trainings'] = $query;
			}
		$this->load->view('trainers_view', $data);
	}

	public function register_trainers(){
			$this->form_validation->set_rules('trainerId', 'Trainer ID', 'required');
			$this->form_validation->set_rules('fname', 'First Name', 'required');
			$this->form_validation->set_rules('sex', 'Sex', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
			$this->form_validation->set_rules('lname', 'Last name', 'required');

			if ($this->form_validation->run()==FALSE){
				$this->load->view('trainers_view');
			}else{
				if($_FILES['userfile']['name'] != '')
				{	   	$config['upload_path']          = './assets/uploads/';
						$config['allowed_types']        = 'jpg|jpeg|png|doc|txt|pdf|doc|docx|sql|zip|rar';
						$config['max_size']             = 100000;
						$config['max_width']            = 100000;
						$config['max_height']           = 100000;
						$this->load->library('upload', $config);
						
					if ( ! $this->upload->do_upload())
					{	
					
				$this->load->view('trainers_view');
					}else{
						$upload_image = $this->upload->data();
   						$data = array('trainerId'       => set_value('trainerId'),
							  'fname'           		=> set_value('fname'),
							  'lname'         			=> set_value('lname'),
							  'sex' 	   				=> set_value('sex'),
							  'age'       				=> set_value('age'),
							  'phone'           		=> set_value('phone'),
							  'city' 					=> set_value('city'),
							  'email'           		=> set_value('email'),
							  'training'           		=> set_value('training'),
							  'experience'           	=> set_value('experience'),
							  'file_address1'			=> $upload_image['file_name'],
							  'specialized_field'       => set_value('specialized_field'),
							  'salary'                	=> set_value('salary'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status')

					);
				$this->Main_model->trainer_register($data);
				
				}
				redirect('Trainers_controller');
			}
		}
}
}