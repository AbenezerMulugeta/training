<?php
class Schedule_view_control_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->select_schedule();
			$data['schedule'] = null;
			if($query){
				$data['schedule'] = $query;
			}
		$this->load->view('schedule_view_control_view', $data);
	}
}