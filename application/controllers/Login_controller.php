<?php
class Login_controller extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function index(){
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run()==false){
			$this->load->view('login_view');
		}else{
			$check_validity = $this->Login_model->login();

			if ($check_validity == false){
				$check_status = $this->Login_model->deactive();
				if ($check_status == false){
					$this->session->set_flashdata('error', 'Incorrect Username/ Password');
				}else{
					$this->session->set_flashdata('error', 'Account Deactivated');
				}
				redirect('Login_controller');
			}else{
				$this->session->set_userdata('username', $check_validity->username);
				$this->session->set_userdata('role', $check_validity->role);
				$this->session->set_userdata('status', $check_validity->status);
			}
			switch($check_validity->role){
				case 'administrator';
				redirect('Login_controller/admin');
				break;
				case'registrar';
				redirect('Login_controller/register');
				break;
				case'controller';
				redirect('Login_controller/control');
				break;
				default;
				break;
			}
		}
		
	}

	public function register(){
		redirect('Register_controller');
	}

	public function admin(){
		redirect('Admin_controller');
	}

	public function control(){
		redirect('Control_controller');
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login_controller');
	}
}