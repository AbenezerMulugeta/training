<?php
class Trainers_view_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->model_trainers_view();
			$data['trainers'] = null;
			if($query){
				$data['trainers'] = $query;
			}
		$this->load->view('trainers_view_view', $data);
	}
}