<?php
class Students_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$query = $this->Main_model->trainings_drop_down();
			if($query){
				$data['trainings'] = $query;
			}

			$query1 = $this->Main_model->trainers_drop_down();
			if($query1){
				$data['trainer'] = $query1;
			}
		$this->load->view('students_view', $data);
	}

	public function register_students(){
			$this->form_validation->set_rules('traineeID', 'Trainee ID', 'required');
			$this->form_validation->set_rules('fName', 'First Name', 'required');
			$this->form_validation->set_rules('sex', 'Sex', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('lName', 'Last name', 'required');
			$this->form_validation->set_rules('age', "Age", 'required');

			if ($this->form_validation->run()==FALSE){
				$this->load->view('students_view');
			}else{
				$data = array('traineeId'         		=> set_value('traineeID'),
							  'fname'           		=> set_value('fName'),
							  'lname'         			=> set_value('lName'),
							  'sex' 	   				=> set_value('sex'),
							  'age'       				=> set_value('age'),
							  'phone'           		=> set_value('phone'),
							  'email' 					=> set_value('email'),
							  'education_level' 		=> set_value('education_level'),
							  'city'           			=> set_value('city'),
							  'company'           		=> set_value('company'),
							  'class'           		=> set_value('class'),
							  'program'           		=> set_value('program'),
							  'schedule_fee'           	=> set_value('schedule_fee'),
							  'starting_date'			=> set_value('starting_date'),
							  'expiration_date'			=> set_value('expiration_date'),
							  'status'					=> set_value('status')
							  
					);
				$this->Main_model->student_register($data);
				redirect('Students_controller');
				
			}
		}
}