<?php
class Trainers_off_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
	}

	public function index(){
		$update = array('status'		=>		'contract_completed');
		$this->Main_model->update_trainers_status($update);
		$query = $this->Main_model->trainers_off();
			$data['trainers'] = null;
			if($query){
				$data['trainers'] = $query;
			}
		$this->load->view('trainers_off_view', $data);
	}
}