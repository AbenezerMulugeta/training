<?php
class Main_model extends CI_Model{
	public function training_register($data){
		$this->db->insert('training', $data);
		$this->db->reconnect();
	}

	public function trainer_register($data){
		$this->db->insert('trainer', $data);
		$this->db->reconnect();
	}

	public function student_register($data){
		$this->db->insert('trainee', $data);
		$this->db->reconnect();
	}

	public function create_account($data){
		$this->db->insert('user', $data);
		$this->db->reconnect();
	}

	public function post($data){
		$this->db->insert('post', $data);
		$this->db->reconnect();
	}

	public function create_schedule($data){
		$this->db->insert('schedule', $data);
		$this->db->reconnect();
	}

	public function trainings_drop_down(){
			$this->db->select("*");
			$this->db->from('training');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function trainers_drop_down(){
			$this->db->select("fname, lname, training");
			$this->db->from('trainer');
			$query1 = $this->db->get();
			return $query1->result();
			$this->db->reconnect();
	}

	public function model_trainings_view(){
			$this->db->select("*");
			$this->db->from('training');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function select_schedule(){
			$this->db->select("*");
			$this->db->from('schedule');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function model_trainings_view_status(){
			$this->db->select("*");
			$this->db->from('training');
			$this->db->where('view', '0');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function model_trainers_view_status(){
			$this->db->select("*");
			$this->db->from('trainer');
			$this->db->where('view', '0');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function model_trainees_view_status(){
			$this->db->select("*");
			$this->db->from('trainee');
			$this->db->where('view', '0');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function view_post(){
			$this->db->select("*");
			$this->db->from('post');
			$this->db->order_by('date', 'desc');
			$this->db->limit('3');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function select_user(){
			$this->db->select("fullname");
			$this->db->from('user');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

		public function model_trainees_view(){
			$this->db->select("*");
			$this->db->from('trainee');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function model_trainers_view(){
			$this->db->select("*");
			$this->db->from('trainer');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function trainings_view_for_update(){
			$this->db->select("*");
			$this->db->from('training');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function update_trainings($update,$select){
			$this->db->where('training', $select);
			$this->db->update('training', $update);
			$this->db->reconnect();
	}

	public function update_trainings_view($update, $course){
			$this->db->where('training', $course);
			$this->db->update('training', $update);
			$this->db->reconnect();
	}

	public function update_trainers_view($update, $trainers){
			$this->db->where('trainerId', $trainers);
			$this->db->update('trainer', $update);
			$this->db->reconnect();
	}

	public function update_students_view($update, $students){
			$this->db->where('traineeId', $students);
			$this->db->update('trainee', $update);
			$this->db->reconnect();
	}

	public function trainers_view_for_update(){
			$this->db->select("*");
			$this->db->from('trainer');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function update_trainers($update,$select){
			$this->db->where('trainerId', $select);
			$this->db->update('trainer', $update);
			$this->db->reconnect();
	}

	public function trainees_view_for_update(){
			$this->db->select("*");
			$this->db->from('trainee');
			$query = $this->db->get();
			return $query->result();
			$this->db->reconnect();
	}

	public function update_trainees($update,$select){
			$this->db->where('traineeId', $select);
			$this->db->update('trainee', $update);
			$this->db->reconnect();
	}

	public function update_students_status($update){
		$this->db->where('expiration_date', date("Y-m-d"));
		$this->db->update('trainee', $update);
		$this->db->reconnect();
	}

	public function update_trainers_status($update){
		$this->db->where('expiration_date', date("Y-m-d"));
		$this->db->update('trainer', $update);
		$this->db->reconnect();
	}

	public function trainers_off(){
			$this->db->select("*");
			$this->db->from('trainer');
			$this->db->where('expiration_date', date("Y-m-d"));
			$query = $this->db->get();
			if (!$query){
				return false;
				$this->db->reconnect();
			}else{
				return $query->result();
				$this->db->reconnect();
				}
				$this->db->reconnect();
	}

	public function students_off(){
			$this->db->select("*");
			$this->db->from('trainee');
			$this->db->where('expiration_date', date("Y-m-d"));
			$query = $this->db->get();
			if (!$query){
				return false;
				$this->db->reconnect();
			}else{
				return $query->result();
				$this->db->reconnect();
				}
	}

	public function remove_schedule($id){
		$this->db->where('id', $id);
		$this->db->delete('schedule');
		$this->db->reconnect();
	}

	public function view_document(){
		$show = $this->db->select('file_address1');
		$show = $this->db->get('trainer');
		if($show->num_rows() > 0 ){
			return $show->result();

		}else{
			return array();
		}
	}
}