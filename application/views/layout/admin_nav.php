<?php $this->load->view('layout/header'); ?>
	<nav class="navbar navbar-default navbar-fixed-top" style="background-color: #6C3483;">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <font color="#D5D8DC"><h3>CREAVERS </font><small><font color="#D5D8DC">Training Center</font></small></h3>
	    </div>
	    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="Admin_controller"><font color="#D5D8DC">Home</font></a></li>
	        <li><a href="Account_controller"><font color="#D5D8DC">Create User Account</font></a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><font color="#D5D8DC">Reports</font> <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="Trainings_report_controller"><i class="icon-edit-sign"></i><i class="fa fa-desktop" aria-hidden="true"></i> Trainings</a></li>
	            <li><a href="Trainers_report_controller"><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Trainers</a></li>
	            <li><a href="Students_report_controller"><i class="icon-edit-user"></i><i class="fa fa-desktop" aria-hidden="true"></i> Students</a></li>
	          </ul>
	          </li>
	          <li><a href="NoticeBoard_controller"><font color="#D5D8DC">Notice Board</font></a></li>
	          <li><a href="Schedule_view_admin_controller"><font color="#D5D8DC">View Schedule</font></a></li>
	          <li><a href="New_controller"><font color="#D5D8DC">Whats New?</font></a></li>
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="Login_controller/logout"><font color="#D5D8DC">Logout</font></a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<style>
		body { padding-top: 80px; }
	</style>