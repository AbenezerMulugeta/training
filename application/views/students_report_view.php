<?php $this->load->view('layout/admin_nav'); ?>

  
  <div class = "container" style="background-color: #F8F9F9;">
  <div class="panel-heading">
      <h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Students Report<h3>
      <hr/>
  </div>
  <div class="panel-body">
    <table class="table" id="view_students">
    <thead>
      <tr>
        <th>Student Id</th>
        <th>Name</th>
        <th>Sex</th>
        <th>Age</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Education Level</th>
        <th>City</th>
        <th>Company</th>
        <th>Class Info</th>
        <th>Period & fee</th>
        <th>Status</th>
        <th>Starting Date</th>
        <th>Expiry date</th>
      </tr>
    </thead>
    <tbody>
    <?php if ($trainees != NULL){?>
    <?php foreach($trainees as $row){ ?>
      <tr>          
        <th style="background-color: white;"><font color = "black"><?= $row->traineeId; ?></th>
        <th style="background-color: white;"><font color = "black"><?= $row->fname." ".$row->lname; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->sex; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->age; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->phone; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->email; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->education_level; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->city; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->company; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->class; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->schedule_fee; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->status; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->starting_date; ?></th>
        <th style="background-color: white;"><font color = "black"><?=$row->expiration_date; ?></th>
      </tr>
      <?php } ?>
      <?php } ?>
    </tbody>
    </table>
  </div>
  </div>

  <script>
    $(document).ready(function(){
      $('#view_students').DataTable();
                  
      });
  </script>
  <style>
    table {
    display: block;
    overflow: scroll;
}
  </style>
    
<?php $this->load->view('layout/footer'); ?>