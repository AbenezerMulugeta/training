<?php $this->load->view('layout/register_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
  <div class="panel-heading">
      <h3><i class="icon-edit-sign"></i><i class="fa fa-book" aria-hidden="true"></i> View Schedule <h3>
      <hr/>
  </div>
  <div class="panel-body">
    <table class="table table-bordered">
    <thead>
      <tr>
        <th>Course</th>
        <th>Mon</th>
        <th>Tue</th>
        <th>Wed</th>
        <th>Thu</th>
        <th>Fri</th>
        <th>Sat</th>
        <th><font color="FF9999">Sun</font></th>
      </tr>
    </thead>
    <tbody>
    <?php if ($schedule != NULL){?>
    <?php foreach($schedule as $row){ ?>
      <tr>
        <th><?=$row->course; ?></th>
        <th style="background-color: #808080;">
          <?php $time = $row->Monday_start;
          if ($time != date('00:00:00')){
            echo $row->Monday_start. " - ";
            } ?><?php $time = $row->Monday_end;
          if ($time != date('00:00:00')){
            echo $row->Monday_end;
            } ?>
        </th>
        <th style="background-color: #999999;">
          <?php $time = $row->Tuesday_start;
          if ($time != date('00:00:00')){
            echo $row->Tuesday_start. " - ";
            } ?> <?php $time = $row->Tuesday_end;
          if ($time != date('00:00:00')){
            echo $row->Tuesday_end;
            } ?>
        </th>
        <th style="background-color: #808080;">
          <?php $time = $row->Wednesday_start;
          if ($time != date('00:00:00')){
            echo $row->Wednesday_start. " - ";
            } ?><?php $time = $row->Wednesday_end;
          if ($time != date('00:00:00')){
            echo $row->Wednesday_end;
            } ?>
        </th>
        <th style="background-color: #999999;">
          <?php $time = $row->Thursday_start;
          if ($time != date('00:00:00')){
            echo $row->Thursday_start. " - ";
            } ?><?php $time = $row->Thursday_end;
          if ($time != date('00:00:00')){
            echo $row->Thursday_end;
            } ?>
        </th>
        <th style="background-color: #808080;">
          <?php $time = $row->Friday_start;
          if ($time != date('00:00:00')){
            echo $row->Friday_start. " - ";
            } ?><?php $time = $row->Friday_end;
          if ($time != date('00:00:00')){
            echo $row->Friday_end;
            } ?>
        </th>
        <th style="background-color: #999999;">
          <?php $time = $row->Saturday_start;
          if ($time != date('00:00:00')){
            echo $row->Saturday_start. " - ";
            } ?><?php $time = $row->Saturday_end;
          if ($time != date('00:00:00')){
            echo $row->Saturday_end;
            } ?>
        </th>
        <th style="background-color: #FF9999;">
          <?php $time = $row->Sunday_start;
          if ($time != date('00:00:00')){
            echo $row->Sunday_start. " - ";
            } ?><?php $time = $row->Sunday_end;
          if ($time != date('00:00:00')){
            echo $row->Sunday_end;
            } ?>
        </th>
      </tr>
      <?php } ?>
      <?php } ?>
    </tbody>
    </table>
    <?php echo validation_errors();?>
    <?php echo form_open_multipart('Schedule_view_controller/remove_schedule'); ?>
        <div class="col-xs-5">
          <select class="form-control" name="course" value="<?php echo set_value('course'); ?>" >
          <option value="none">-- Select Course --</option>
          <?php if ($schedule != NULL){?>
          <?php foreach($schedule as $row){ ?>
          <option value='<?= $row->id; ?>'><?= $row->course; ?></option>
          <?php } ?>
          <?php } ?>
          </select>
        </div>
        <button type="submit" class="btn btn-danger">Remove</button>
    <?php echo form_close(); ?>
  </div>
  </div>

    
<?php $this->load->view('layout/footer'); ?>