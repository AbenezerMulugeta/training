<?php $this->load->view('layout/admin_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Create Staff Account <h3>
    	<hr/>
	</div>
	<div class="panel-body">
		<?php echo validation_errors();?>
		<?php echo form_open_multipart('Account_controller/create_account'); ?>
  		<div class="col-xs-5">
    		<label for="fName">Fullname  </label>
    		<input type="text" class="form-control" name="fname" value="<?php echo set_value('fname') ?>">
    		<br>	
    	</div>
    	<br><br><br><br><br>
    	<div class="col-xs-5">
		    <label for="role">Role</label>
		    <select class="form-control" name="role" value="<?php echo set_value('role'); ?>" >
		          <option value="none">-- Select Staff Role --</option>
		          <option value="administrator">Administrator</option>
		          <option value="registrar">Registrar</option>
		          <option value="controller">Controller</option>
		    </select>
		    <br>
  		</div>
  		<br><br><br><br><br>
    	<div class="col-xs-5">
    		<label for="username">Userame  </label>
		    <input type="text" class="form-control" name="username" value="<?php echo set_value('username') ?>">
		    <br>
    	</div>
    	<br><br><br><br><br>
  		<div class="col-xs-5">
		    <label for="password">Password  </label>
		    <input type="password" class="form-control " id='myPassword' name="password" value="<?php echo set_value('password') ?>">
		    <br>
    	</div>
    	<br><br><i class="icon-edit-sign"></i><i class="fa fa-eye" aria-hidden="true" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();"></i>
      <br><br><br>
  <div class="col-xs-5">
  <center><button type="submit" class="btn btn-success">Register</button></center>
  <br>
  </div>
  </div>
		<?php echo form_close(); ?>
    <script>
      function mouseoverPass(obj) {
          var obj = document.getElementById('myPassword');
          obj.type = "text";
        }
      function mouseoutPass(obj) {
          var obj = document.getElementById('myPassword');
          obj.type = "password";
        }

</script>

		
<?php $this->load->view('layout/footer'); ?>