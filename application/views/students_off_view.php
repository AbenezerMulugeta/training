<?php $this->load->view('layout/control_nav'); ?>

  
  <div class = "container" style="background-color: #F8F9F9;">
  <div class="panel-heading">
      <h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> List Of Students <h3>
      <hr/>
  </div>
  <div class="panel-body">
    <table class="table" id="view_students">
    <thead>
      <tr>
        <th>Student Id</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Class Info</th>
        <th>Starting Date</th>
        <th>Expiry date</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    <?php if ($trainees != NULL){?>
    <?php foreach($trainees as $row){ ?>
      <tr>                    
        <th><font color = "black"><?= $row->traineeId; ?></th>
        <th><font color = "black"><?= $row->fname." ".$row->lname; ?></th>
        <th><font color = "black"><?=$row->phone; ?></th>
        <th><font color = "black"><?=$row->email; ?></th>
        <th><font color = "black"><?=$row->class; ?></th>
        <th><font color = "black"><?=$row->starting_date; ?></th>
        <th><font color = "black"><?=$row->expiration_date; ?></th>
        <th><font color = "black"><?=$row->status; ?></th>
      </tr>
      <?php } ?>
      <?php } ?>
    </tbody>
    </table>
  </div>
  </div>

  <script>
    $(document).ready(function(){
      $('#view_students').DataTable();
                  
      });
  </script>
    
<?php $this->load->view('layout/footer'); ?>