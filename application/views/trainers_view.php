<?php $this->load->view('layout/register_nav'); ?>
<div class='container' style="background-color: #F8F9F9;">
<div class="panel-heading">
    <h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Trainers Registration Form <h3>
    <hr/>
</div>
	<?php echo validation_errors();?>
	<?php echo form_open_multipart('Trainers_controller/register_trainers'); ?>
	<div class="panel-body">
  <div class="col-xs-8">
  <span id="date-label-from" class="date-label">Contract Starting Date: </span><input class="date_range_filter date" type="date" name="starting_date" />
  <span id="date-label-from" class="date-label">Contract Expiry Date: </span><input class="date_range_filter date" type="date" name="expiration_date" />
  </div>
  <br><br>
  <div class="col-xs-3">
  <br>
  <label for="trainerId">Trainer ID  </label>
    <input type="text" class="form-control " name="trainerId" value="<?php echo set_value('trainerId') ?>">
    </div>
    <br><br><br><br><br>
  <div class="col-xs-6">
    <label for="fName">First name  </label>
    <input type="text" class="form-control" name="fname" value="<?php echo set_value('fname') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="lname">Last name  </label>
    <input type="text" class="form-control" name="lname" value="<?php echo set_value('lname') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="sex">Sex  </label>
    <div class="radio">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="sex" value="<?php echo "Male"; ?>" checked="checked" />Male &nbsp;&nbsp;&nbsp;<br><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="sex" value="<?php echo "female"; ?>" />Female
      </div>
      <br>
      </div>
      <div class="col-xs-6">
      <label for="age">Age  </label>
    <input type="number" class="form-control" name="age" placeholder="20 - 60" value="<?php echo set_value('age') ?>" min='20' max='60'>
    <br>
    </div>
      <div class="col-xs-6">
    <label for="username">Phone  </label>
    <input type="text" class="form-control" name="phone" value="<?php echo set_value('phone') ?>">
    <br>
    </div>
    <div class="col-xs-6">
    <label for="username">Email  </label>
    <input type="text" class="form-control" placeholder="eg. contact@email.com" name="email" value="<?php echo set_value('email') ?>">
    <br>
    </div>
    <div class="col-xs-6">
        <label for="sel1">Course </label>
          <select class="form-control" name="training" value="<?php echo set_value('training'); ?>" >
          <option value="none">-- Select Course --</option>
          <?php foreach($trainings as $row){ ?>
          <option value='<?= $row->training; ?>'><?= $row->training; ?> for <?= $row->specify_schedule; ?></option>
          <?php } ?>
       
          </select>
          <br>
          </div>
    <div class="col-xs-6">

    <label for="username">City  </label>
    <select class="form-control" name="city" value="<?php echo set_value('city'); ?>" >
          <option value="none">-- Select City --</option>
          <option value="Addis Ababa">Addis Ababa</option>
          <option value="Other">Other</option>
       
          </select>
    <br>
    </div>
    
    <div class="col-xs-6">
    <label for="experience">Work Experience  </label>
    <input type="number" class="form-control" name="experience" value="<?php echo set_value('experience') ?>" min='0' max='50'>
    <br>
    </div>
    <div class="col-xs-6">
    <label for="experience">Specialization </label>
    <input type="text" class="form-control" name="specialized_field" value="<?php echo set_value('specialized_field') ?>">
    <br>
    </div>
    <div class="col-xs-6">

    <label for="username">Salary  </label>
    <select class="form-control" name="salary" value="<?php echo set_value('salary'); ?>" >
          <option value="none">-- Select salary --</option>
          <option value="700 birr">Fee Per Hour</option>
       
          </select>
    <br>
    </div>
    <div class="col-xs-6">

    <label for="username">Status</label>
    <select class="form-control" name="status" value="<?php echo set_value('status'); ?>" >
          <option value="none">-- Select Trainer Status --</option>
          <option value="on_contract">On Contract</option>
          <option value="available">Available</option>
          <option value="contract_completed">Contract Completed</option>
          <option value="unavailable">Unavailable</option>
       
          </select>
    <br>
    </div>
    <div class="col-xs-6">
    <label for="experience">Upload Supporting Document</label>
    <input class="form-control" type="file" name="userfile" class="form-control">
    <br>
    <font color="red">If you are uploading multiple files make sure you compress the file first.<br>
    Rename files using the trainer name before uploading.</font>
    </div>
  </div>
  <center><button type="submit" class="btn btn-success">Register</button></center>
<br>
<?php echo form_close(); ?>
</div>
<?php $this->load->view('layout/footer'); ?>