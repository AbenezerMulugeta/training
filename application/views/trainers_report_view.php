<?php $this->load->view('layout/admin_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Trainers Report <h3>
    	<hr/>
	</div>
	<div class="panel-body">
		<table class="table" id="view_trainers">
		<thead>
			<tr>
				<th>Trainer ID</th>
				<th>Name</th>
				<th>Sex</th>
				<th>Age</th>
				<th>Phone</th>
				<th>Email</th>
				<th>City</th>
				<th>Training</th>
				<th>Experience</th>
				<th>Specialization</th>
				<th>Salary</th>
				<th>Starting Date</th>
				<th>Expiry Date</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($trainers != NULL){?>
		<?php foreach($trainers as $row){ ?>
			<tr>
										
				<th style="background-color: white;"><font color = "black"><?= $row->trainerId; ?></th>
				<th style="background-color: white;"><font color = "black"><?= $row->fname." ".$row->lname; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->sex; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->age; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->phone; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->email; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->city; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->training; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->experience; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->specialized_field; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->salary; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->starting_date; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->expiration_date; ?></th>
				<th style="background-color: white;"><font color = "black"><?=$row->status; ?></th>
			</tr>
			<?php } ?>
			<?php } ?>
		</tbody>
		</table>
	</div>
	</div>

	<script>
		$(document).ready(function(){
			$('#view_trainers').DataTable();
									
			});
	</script>
	 <style>
    table {
    display: block;
    overflow: scroll;
}
  </style>
		
<?php $this->load->view('layout/footer'); ?>