<?php $this->load->view('layout/register_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-book" aria-hidden="true"></i> Course Schedule <h3>
    	<hr/>
	</div>
	<div class="panel-body">
    <?php echo validation_errors();?>
    <?php echo form_open_multipart('Schedule_controller/create_schedule'); ?>
    <div class="col-xs-10">
      <div class="col-xs-5">
        <label for="sel1">Course</label>
          <select class="form-control" name="course" value="<?php echo set_value('course'); ?>" >
          <option value='none'>-- Select Course --</option>
          <?php foreach($trainings as $row1){ ?>
          <option value='<?=$row1->training; ?> for <?=$row1->specify_schedule; ?>'><?=$row1->training; ?> for <?=$row1->specify_schedule; ?></option>
          <?php } ?>
          </select> 
    </div>
    </div>
    <div class="col-xs-10">
    <br><br>
    <div class="col-xs-2">
    <br><br>
    <label for="trainerId">Monday</label>
    </div>
    
    
    <div class="col-xs-4">
    <label for="trainerId">Starting Time</label>
    <input type="time" class="form-control " name="course1_start" value="<?php echo set_value('course1_start') ?>">
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Ending Time</label>
    <input type="time" class="form-control " name="course1_end" value="<?php echo set_value('course1_end') ?>">
    </div>
    <br><br><br><br>
    <div class="col-xs-2">
    <br><br>
    <label for="trainerId">Tuesday</label>
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Starting Time</label>
    <input type="time" class="form-control " name="course2_start" value="<?php echo set_value('course2_start') ?>">
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Ending Time</label>
    <input type="time" class="form-control " name="course2_end" value="<?php echo set_value('course2_end') ?>">
    </div>
    <br><br><br><br>
    <div class="col-xs-2">
    <br><br>
    <label for="trainerId">Wednesday</label>
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Starting Time</label>
    <input type="time" class="form-control " name="course3_start" value="<?php echo set_value('course3_start') ?>">
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Ending Time</label>
    <input type="time" class="form-control " name="course3_end" value="<?php echo set_value('course3_end') ?>">
    </div>
    <br><br><br><br>
    <div class="col-xs-2">
    <br><br>
    <label for="trainerId">Thursday</label>
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Starting Time</label>
    <input type="time" class="form-control " name="course4_start" value="<?php echo set_value('course4_start') ?>">
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Ending Time</label>
    <input type="time" class="form-control " name="course4_end" value="<?php echo set_value('course4_end') ?>">
    </div>
    <br><br><br><br>
    <div class="col-xs-2">
    <br><br>
    <label for="trainerId">Friday</label>
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Starting Time</label>
    <input type="time" class="form-control " name="course5_start" value="<?php echo set_value('course5_start') ?>">
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Ending Time</label>
    <input type="time" class="form-control " name="course5_end" value="<?php echo set_value('course5_end') ?>">
    </div>
    <br><br><br><br>
    <div class="col-xs-2">
    <br><br>
    <label for="trainerId">Saturday</label>
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Starting Time</label>
    <input type="time" class="form-control " name="course6_start" value="<?php echo set_value('course6_start') ?>">
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Ending Time</label>
    <input type="time" class="form-control " name="course6_end" value="<?php echo set_value('course6_end') ?>">
    </div>
    <br><br><br><br>
    <div class="col-xs-2">
    <br><br>
    <label for="trainerId">Sunday</label>
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Starting Time</label>
    <input type="time" class="form-control " name="course7_start" value="<?php echo set_value('course7_start') ?>">
    </div>
    <div class="col-xs-4">
    <label for="trainerId">Ending Time</label>
    <input type="time" class="form-control " name="course7_end" value="<?php echo set_value('course7_end') ?>">
    </div>
    <div class="col-xs-10">
    <br>
    <button type="submit" class="btn btn-success">Create</button>
    </div>
    </div>
    <?php echo form_close(); ?>
  </div>
  </div>    

		
<?php $this->load->view('layout/footer'); ?>