<?php $this->load->view('layout/admin_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-paperclip" aria-hidden="true"></i> Notice Dashboard <h3>
    	<hr/>
	</div>
	<div class="panel-body">
		<?php echo validation_errors();?>
		<?php echo form_open_multipart('NoticeBoard_controller/post'); ?>
			<div class="col-xs-12">
    			<label for="subject">Title</label>
    			<input type="text" class="form-control " id='title' name="title" value="<?php echo set_value('title') ?>">
    		</div>
    		<br><br><br><br><br>
    		<div class="col-xs-12">
    		<div class="form-group">
  				<label for="message">Message</label>
  				<textarea class="form-control" placeholder = 'Please, make sure you have pressed "ENTER" key after every word that comes before the end of this message box.' name="message" rows="15" id="message"></textarea>
			</div>
			</div>
			<br><br><br><br><br><br><br><br><br>
			<div class="col-xs-4">
			<button type="submit" class="btn btn-success">Post</button>
			</div>
		<?php echo form_close(); ?>
	</div>
		
<?php $this->load->view('layout/footer'); ?>