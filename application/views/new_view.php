<?php $this->load->view('layout/admin_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Recently Added Courses <h3>
    	<hr/>
	</div>
	<div class="panel-body">
  <?php echo validation_errors();?>
  <?php echo form_open_multipart('New_controller/update_trainings'); ?>
  <div class="col-xs-3">

        <label for="sel1">Courses</label>
          <select class="form-control" name="course" value="<?php echo set_value('course'); ?>" >
          <option value='none'>-- Select Course --</option>
          <?php foreach($trainings as $row1){ ?>
          <option value='<?=$row1->training; ?>'><?=$row1->training; ?></option>
          <?php } ?>
          </select> 
    </div>
    <div class="col-xs-10">
    <br>
  <button type="submit" class="btn btn-success">Approve Courses</button>
  </div>
  <?php echo form_close(); ?>
  <br><br><br><br><br><br><br>
		<table class="table" id="view_trainings">
    <thead>
      <tr>
        <th>No</th>
        <th>Course</th>
        <th>Course Description</th>
        <th>Vendor</th>
        <th>Period</th>
        <th>Fee</th>
      </tr>
    </thead>
    <tbody>
    <?php if ($trainings != NULL){?>
    <?php foreach($trainings as $row){ ?>
      <tr>
        <th><?=$row->training_id; ?></th>
        <th><?=$row->training; ?></th>
        <th><?=$row->course_description; ?></th>
        <th><?=$row->vendor; ?></th>
        <th><?=$row->specify_schedule; ?></th>
        <th><?=$row->fee; ?></th>
      </tr> 
    <?php } ?>
    <?php } ?>
    </tbody>
    </table>
  </div>
  

  <div class="panel-heading">
      <h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Recently Added Trainers <h3>
      <hr/>
  </div>
  <div class="panel-body">
  <?php echo validation_errors();?>
  <?php echo form_open_multipart('New_controller/update_trainers'); ?>
  <div class="col-xs-3">

        <label for="sel1">Courses</label>
          <select class="form-control" name="trainers" value="<?php echo set_value('trainers'); ?>" >
          <option value='none'>-- Select Trainer --</option>
          <?php foreach($trainers as $row1){ ?>
          <option value='<?=$row1->trainerId; ?>'><?=$row1->fname; ?> <?=$row1->lname; ?></option>
          <?php } ?>
          </select> 
    </div>
    <div class="col-xs-10">
    <br>
  <button type="submit" class="btn btn-success">Approve Trainers</button>
  </div>
  <?php echo form_close(); ?>
  <br><br><br><br><br><br><br>
    <table class="table" id="view_trainers">
    <thead>
      <tr>
        <th>Trainer ID</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Training</th>
        <th>Starting Date</th>
        <th>Expiry Date</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    <?php if ($trainers != NULL){?>
    <?php foreach($trainers as $row){ ?>
      <tr>      
        <th><font color = "black"><?= $row->trainerId; ?></th>
        <th><font color = "black"><?= $row->fname." ".$row->lname; ?></th>
        <th><font color = "black"><?=$row->phone; ?></th>
        <th><font color = "black"><?=$row->email; ?></th>
        <th><font color = "black"><?=$row->training; ?></th>
        <th><font color = "black"><?=$row->starting_date; ?></th>
        <th><font color = "black"><?=$row->expiration_date; ?></th>
        <th><font color = "black"><?=$row->status; ?></th>
      </tr>
      <?php } ?>
      <?php } ?>
    </tbody>
    </table>
  </div>

  <div class="panel-heading">
      <h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Recently Added Students <h3>
      <hr/>
  </div>
  <div class="panel-body">
  <?php echo validation_errors();?>
  <?php echo form_open_multipart('New_controller/update_students'); ?>
  <div class="col-xs-3">

        <label for="sel1">Courses</label>
          <select class="form-control" name="students" value="<?php echo set_value('students'); ?>" >
          <option value='none'>-- Select Student --</option>
          <?php foreach($trainees as $row1){ ?>
          <option value='<?=$row1->traineeId; ?>'><?=$row1->fname; ?> <?=$row1->lname; ?></option>
          <?php } ?>
          </select> 
    </div>
    <div class="col-xs-10">
    <br>
  <button type="submit" class="btn btn-success">Approve Students</button>
  </div>
  <?php echo form_close(); ?>
  <br><br><br><br><br><br><br>
    <table class="table" id="view_students">
    <thead>
      <tr>
        <th>Student Id</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Education Level</th>
        <th>Class Info</th>
        <th>Starting Date</th>
        <th>Expiry date</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    <?php if ($trainees != NULL){?>
    <?php foreach($trainees as $row){ ?>
      <tr>                    
        <th><font color = "black"><?= $row->traineeId; ?></th>
        <th><font color = "black"><?= $row->fname." ".$row->lname; ?></th>
        <th><font color = "black"><?=$row->phone; ?></th>
        <th><font color = "black"><?=$row->email; ?></th>
        <th><font color = "black"><?=$row->education_level; ?></th>
        <th><font color = "black"><?=$row->class; ?></th>
        <th><font color = "black"><?=$row->starting_date; ?></th>
        <th><font color = "black"><?=$row->expiration_date; ?></th>
        <th><font color = "black"><?=$row->status; ?></th>
      </tr>
      <?php } ?>
      <?php } ?>
    </tbody>
    </table>
  </div>

  </div>

  <script>
    $(document).ready(function(){
      $('#view_trainings').DataTable();
                  
      });
  </script>

  <script>
    $(document).ready(function(){
      $('#view_trainers').DataTable();
                  
      });
  </script>

<script>
    $(document).ready(function(){
      $('#view_students').DataTable();
                  
      });
  </script>
  

		
<?php $this->load->view('layout/footer'); ?>