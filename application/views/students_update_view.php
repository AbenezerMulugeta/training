<?php $this->load->view('layout/register_nav'); ?>
<div class='container' style="background-color: #F8F9F9;">
<div class="panel-heading">
    <h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> Students Update Form <h3>
    <hr/>
</div>
	<?php echo validation_errors();?>
	<?php echo form_open_multipart('Students_update_controller/update_students'); ?>
	<div class="panel-body">
    	<div class="col-xs-6">
        <label for="sel1">Select Student</label>
          <select class="form-control" name="select_trainees" value="<?php echo set_value('select_trainees'); ?>" >
          <option value="none">-- Select Student --</option>
          <?php foreach($trainees_update as $row){ ?>
          <option value='<?= $row->traineeId; ?>'><?= $row->fname; ?> <?= $row->lname; ?></option>
          <?php } ?>
          </select>
          <br>
          </div>
          <br><br><br><br><br><br>

          <div class="col-xs-8">
    <span id="date-label-from" class="date-label">Registration Date: </span><input class="date_range_filter date" type="date" name="starting_date" />
  <span id="date-label-from" class="date-label">Expiry Date: </span><input class="date_range_filter date" type="date" name="expiration_date" />
</div>
<br><br>

          <div class="col-xs-6">

    <label for="username">Phone  </label>
    <input type="text" class="form-control" name="phone" value="<?php echo set_value('phone') ?>">
    <br>
    </div>
        <div class="col-xs-6">

    <label for="username">Email  </label>
    <input type="text" class="form-control" placeholder="eg. contact@email.com" name="email" value="<?php echo set_value('email') ?>">
    <br>
    </div>
    
  <div class="col-xs-6">

    <label for="lName">Education Level  </label>
    <select class="form-control" name="education_level" value="<?php echo set_value('education_level'); ?>" >
          <option value="none">-- Select Education Level --</option>
          <option value="Undergraduate">Undergraduate</option>
          <option value="Bachelor Degree">Bachelor's Degree</option>
          <option value="Mater Degree">Master's Degree</option>
          <option value="Doctrate Degree">Doctrate's Degree</option>
          <option value="Other">Other</option>
       
          </select>
    <br>
    </div>
    
        
        <div class="col-xs-6">

    <label for="username">City  </label>
    <select class="form-control" name="city" value="<?php echo set_value('city'); ?>" >
          <option value="none">-- Select City --</option>
          <option value="Addis Ababa">Addis Ababa</option>
          <option value="Other">Other</option>
       
          </select>
    <br>
    </div>
        <div class="col-xs-6">

    <label for="lName">Company  </label>
    <input type="text" class="form-control" name="company" value="<?php echo set_value('company') ?>">
    <br>
    </div>
    
    <div class="col-xs-6">

        <label for="sel1">Class Information</label>
        

          <select class="form-control" name="class" value="<?php echo set_value('class'); ?>" >
          <option value='none'>-- Select Class --</option>
          <?php foreach($trainer as $row1){ ?>
          <option value='<?= $row1->fname; ?> <?= $row1->lname; ?>: <?= $row1->training; ?>'><?= $row1->fname; ?> <?= $row1->lname; ?>: <?= $row1->training; ?></option>
          <?php } ?>
          </select>
          
          <br>
    </div>
    <div class="col-xs-6">

        <label for="sel1">Period & Fee</label>
        

          <select class="form-control" name="schedule_fee" value="<?php echo set_value('schedule_fee'); ?>" >
          <option value='none'>-- Select Period & fee --</option>
          <?php foreach($trainings as $row){ ?>
          <option value='<?= $row->training; ?> for <?= $row->specify_schedule; ?>: <?= $row->fee; ?>'><?= $row->training; ?> for <?= $row->specify_schedule; ?>: <?= $row->fee; ?></option>
          <?php } ?>
       
          </select>
          
          <br>
    </div>
    <div class="col-xs-6">

        <label for="sel1">Program</label>
        

          <select class="form-control" name="program" value="<?php echo set_value('program'); ?>" >
          <option value='none'>-- Select Program --</option>
          <?php if ($trainings != NULL){ ?>
          <?php foreach($trainings as $row){ ?>
          <option value='<?= $row->program; ?>'><?= $row->training; ?> ( <?= $row->program; ?> )</option>
          <?php } ?>
       <?php } ?>
          </select>
          
          <br>
    </div>
    <div class="col-xs-6">

    <label for="username">Status</label>
    <select class="form-control" name="status" value="<?php echo set_value('status'); ?>" >
          <option value="none">-- Select Student Status --</option>
          <option value="on_contract">On Contract</option>
          <option value="contract_completed">Contract Completed</option>
       
          </select>
    <br>
    </div>
      
    </div>
    <br>
  <center><center><button type="submit" class="btn btn-success">Update</button></center>
  <br>
  </div>
    </div>
<?php echo form_close(); ?>
</div>
<?php $this->load->view('layout/footer'); ?>