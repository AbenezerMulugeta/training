<?php $this->load->view('layout/register_nav'); ?>
<div class='container' style="background-color: #F8F9F9;">
<div class="panel-heading">
    <h3><i class="icon-edit-sign"></i><i class="fa fa-desktop" aria-hidden="true"></i> Training Update Form <h3>
    <hr/>
</div>
	<?php echo validation_errors();?>
	<?php echo form_open_multipart('Trainings_update_controller/update_trainings'); ?>
	<div class="panel-body">
    <div class="col-xs-6">
        <label for="sel1">Select Training</label>
          <select class="form-control" name="select_training" value="<?php echo set_value('select_training'); ?>" >
          <option value="none">-- Select Trainings --</option>
          <?php foreach($trainings_update as $row){ ?>
          <option value='<?= $row->training; ?>'><?= $row->training; ?> for <?= $row->specify_schedule; ?></option>
          <?php } ?>
          </select>
          </div>
          <br><br><br><br><br>
          <div class="col-xs-6">

    <label for="trainerId">Period </label>
      <input type="text" class="form-control" name="period" value="<?php echo set_value('period') ?>">
      <br>
      </div>
      <div class="col-xs-6">
      <label for="trainerId">Program </label><br>
      <div class="radio">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="program" value="morning" checked="checked" />Morning &nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="program" value="mid_day" />Mid Day&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="program" value="evening" />Evening
        <br><br>
      </div>
    </div>
      <div class="col-xs-6">

    <label for="trainerId">Fee </label>
      <input type="text" class="form-control" name="fee" value="<?php echo set_value('fee') ?>">
      <br>
      </div>
      
      </div>
    <center><center><button type="submit" class="btn btn-success">Update</button></center>
    <br>
    </div>
<?php echo form_close(); ?>
</div>
<?php $this->load->view('layout/footer'); ?>