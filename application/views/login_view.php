<?php $this->load->view('layout/header'); ?>

    <div class="container" style="margin-top:100px;">
    <div class="panel-heading">
            <center><h1><font color="#6C348">CREAVERS Training Center </font><h1></center>
            </div>
    <div class="row">
      <div class="col-md-4"></div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body">
            <?php
            if(validation_errors()){
            ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><?php echo validation_errors(); ?></strong>
            </div>
            <?php
            }
            ?>
              <?= form_open('Login_controller') ?>
                <div class="form-group">
                  <label for="username">User Name</label>
                  <input type="text" class="form-control" name="username" >
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" name="password" >
                </div>
                <div class="form-group">
                <div class="col-md-4"></div>
                <div class="col-md-7">
                  <button type="submit" class="btn btn-success">Login</button>
                  </div>
                </div>
              <?= form_close() ?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>

    </div>

    <?php $this->load->view('layout/footer'); ?>