<?php $this->load->view('layout/register_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-user" aria-hidden="true"></i> List Of Trainers <h3>
    	<hr/>
	</div>
	<div class="panel-body">
		<table class="table" id="view_trainers">
		<thead>
			<tr>
				<th>Trainer ID</th>
				<th>Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Training</th>
				<th>Experience</th>
				<th>Specialization</th>
				<th>Salary</th>
				<th>Starting Date</th>
				<th>Expiry Date</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($trainers != NULL){?>
		<?php foreach($trainers as $row){ ?>
			<tr>
										
				<th><font color = "black"><?= $row->trainerId; ?></th>
				<th><font color = "black"><?= $row->fname." ".$row->lname; ?></th>
				<th><font color = "black"><?=$row->phone; ?></th>
				<th><font color = "black"><?=$row->email; ?></th>
				<th><font color = "black"><?=$row->training; ?></th>
				<th><font color = "black"><?=$row->experience; ?></th>
				<th><font color = "black"><?=$row->specialized_field; ?></th>
				<th><font color = "black"><?=$row->salary; ?></th>
				<th><font color = "black"><?=$row->starting_date; ?></th>
				<th><font color = "black"><?=$row->expiration_date; ?></th>
				<th><font color = "black"><?=$row->status; ?></th>


			</tr>
			<?php } ?>
			<?php } ?>
		</tbody>
		</table>
	</div>
	</div>

	<script>
		$(document).ready(function(){
			$('#view_trainers').DataTable();
									
			});
	</script>
		
<?php $this->load->view('layout/footer'); ?>