<?php $this->load->view('layout/admin_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-desktop" aria-hidden="true"></i> Trainings Report <h3>
    	<hr/>
	</div>
	<div class="panel-body">
		<table class="table" id="view_trainings">
		<thead>
			<tr>
				<th>No</th>
				<th>Course</th>
				<th>Course Description</th>
				<th>Vendor</th>
				<th>Period</th>
				<th>Fee</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($trainings != NULL){?>
		<?php foreach($trainings as $row){ ?>
			<tr>
				<th><?=$row->training_id; ?></th>
				<th><?=$row->training; ?></th>
				<th><?=$row->course_description; ?></th>
				<th><?=$row->vendor; ?></th>
				<th><?=$row->specify_schedule; ?></th>
				<th><?=$row->fee; ?></th>
			</tr> 
		<?php } ?>
		<?php } ?>
		</tbody>
		</table>
	</div>
	</div>
	<script>
		$(document).ready(function(){
			$('#view_trainings').DataTable();
									
			});
	</script>
	
		
<?php $this->load->view('layout/footer'); ?>