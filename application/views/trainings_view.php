<?php $this->load->view('layout/register_nav'); ?>
<div class='container' style="background-color: #F8F9F9;">
<div class="panel-heading">
    <h3><i class="icon-edit-sign"></i><i class="fa fa-desktop" aria-hidden="true"></i> Course Registration Form <h3>
    <hr/>
</div>
	<?php echo validation_errors();?>
	<?php echo form_open_multipart('Trainings_controller/register_trainings'); ?>
	<div class="panel-body">
    <div class="col-xs-3">
    	<label for="trainerId">Course ID  </label>
    	<input type="number" class="form-control " id='trainingID' name="trainingID" value="<?php echo set_value('traineeID') ?> min='1' max='2'">
    	
    </div>
    <br><br><br><br>
	<div class="col-xs-6">
      	<label for="trainerId">Course Name</label>
      	<input type="text" class="form-control " id='ex1' name="training" value="<?php echo set_value('training') ?>">
      	<br>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
          <label for="message">Course Description</label>
          <textarea class="form-control" placeholder = 'Please, make sure you have pressed "ENTER" key after every word that comes before the end of this message box.' name="course_description" rows="6" id="course_description"></textarea>
      </div>
      </div>
   	<div class="col-xs-6">
      	<label for="trainerId">Vendor</label>
      	<input type="text" class="form-control " id='ex1' name="vendor" value="<?php echo set_value('vendor') ?>">
      	<br>
    </div>
    <div class="col-xs-6">
    	<label for="trainerId">Period </label>
      	<input type="text" class="form-control" name="specify_schedule" value="<?php echo set_value('specify_schedule') ?>">
      	<br>
    </div>
    <div class="col-xs-6">
      <label for="trainerId">Program </label><br>
      <div class="radio">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="program" value="morning" checked="checked" />Morning &nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="program" value="mid_day" />Mid Day&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="inlineCheckbox1" name="program" value="evening" />Evening
      </div>
    </div>
    <div class="col-xs-6">
    	<label for="trainerId">Fee </label>
      	<input type="text" class="form-control" name="fee" value="<?php echo set_value('fee') ?>">
      	<br>
    </div>
    <center><button type="submit" class="btn btn-success">Register</button></center>
  <br>

    
<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>