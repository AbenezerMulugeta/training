<?php $this->load->view('layout/register_nav'); ?>

	
	<div class = "container" style="background-color: #F8F9F9;">
	<div class="panel-heading">
    	<h3><i class="icon-edit-sign"></i><i class="fa fa-file" aria-hidden="true"></i> Documents of Trainers <h3>
    	<hr/>
	</div>
	<div class="panel-body">
	

	<table class="table" id="view_trainers">
		<thead>
			<tr>
				<th>File</th>
			</tr>
		</thead>
		<tbody>
		<?php if ($view_doc != NULL){?>
		<?php foreach($view_doc as $view):?>
			<tr>				
				<th> <?php echo $view->file_address1 ?>
				<style>#g {width:50px;height: 50px;}</style>
				<a href="<?php echo base_url('/assets/uploads/'.$view->file_address1);?>" >
	            <?php 
	            $my_document =['src'  =>'assets/uploads/'.$view->file_address1, 'class'=>'img-responsive img-portfolio img-hover', 'id'=>'g'];
	            echo img($my_document);
	            ?>
				</a>
				<br> 
				</th>
			</tr>
			<?php endforeach;?>
			<?php } ?>
		</tbody>
		</table>
	</div>
	</div>
<script>
		$(document).ready(function(){
			$('#view_trainers').DataTable();
									
			});
	</script>
<?php $this->load->view('layout/footer'); ?>