-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2017 at 01:52 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `training_id` int(11) NOT NULL,
  `training` varchar(100) NOT NULL,
  `course_description` text NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `specify_schedule` varchar(255) NOT NULL,
  `program` varchar(50) NOT NULL,
  `fee` double NOT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`training_id`, `training`, `course_description`, `vendor`, `specify_schedule`, `program`, `fee`, `view`) VALUES
(1, 'Ms Exchange', '', 'Microsoft', '3 months', '', 6000, 1),
(2, 'SharePoint', '', 'Microsoft', '3 months', '', 2000, 1),
(3, 'SQL Server', '', 'Microsoft', '3 months', '', 2000, 1),
(4, 'MS Servers', '', 'Microsoft', '3 months', '', 2000, 1),
(5, 'Ms Office 365', '', 'Microsoft', '1 months', '', 1, 1),
(6, 'CCNA', '', 'Cisco', '4 months', '', 2000, 1),
(7, 'CCNP', '', 'Cisco', '3 months', '', 6000, 1),
(8, 'HCNA', '', 'Huawei', '3 months', '', 1000, 1),
(9, 'HCNP', '', 'Huawei', '6 months', '', 2500, 1),
(10, 'A+', '', 'Comp TIA', '2 months', '', 2000, 1),
(11, 'Network+', '', 'Comp TIA', '4 months', '', 4000, 1),
(12, 'Security+', '', 'Comp TIA', '3 months', '', 2500, 1),
(13, 'PMP Training', '', 'PMI', '2 months', '', 2600, 1),
(14, 'Wordpress', '', 'Open Source', '1 months', '', 1500, 1),
(15, 'Joomla', '', 'Open Source', '1 month', '', 1000, 1),
(16, 'Drupal', '', 'Open Source', '2 months', '', 2500, 1),
(17, 'Adobe Photoshop', '', 'Adobe', '2 months', '', 4000, 1),
(18, 'Adobe Illustrator', '', 'Adobe', '2 months', '', 3500, 1),
(19, 'CCDP', '', 'Cisco', '4 months', '', 5500, 1),
(20, 'CCDA', '', 'Cisco', '6 months', '', 8000, 1),
(21, 'MCSE Private Cloud', '', 'Microsoft', '3 months', '', 2500, 1),
(22, 'MCSA', '', 'Microsoft', '2 months', '', 1500, 1),
(23, 'MCTS', '', 'Microsoft', '3 months', '', 4000, 1),
(24, 'SQL Server', '', '', '3 months', '', 2000, 1),
(25, 'VMware', '', '', '1 month', '', 1200, 1),
(26, 'Citrix XenServer', '', '', '3 months', '', 2500, 1),
(27, 'Citrix XenApp', '', '', '4 months', '', 3000, 1),
(28, 'Citrix XenDesktop', '', '', '1 month', '', 2000, 1),
(29, 'Software Programming for Begginers', '', 'Microsoft', '1 month', '', 2000, 0),
(30, 'Introduction to Networking and Cabling', '', '', '1 month', '', 2000, 0),
(31, 'Website Designing for Begginers', '', 'Open Source', '1 month', '', 2000, 0),
(32, 'Security Camera Basics', '', '', '1 month', '', 2000, 0),
(33, 'Project Management Knowledge Areas', '', 'PMI', '1 month', '', 2000, 0),
(34, 'Social Media for Business', '', 'Facebook', '1 month', '', 2000, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`training_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
