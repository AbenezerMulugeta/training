-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2017 at 01:52 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `trainer`
--

CREATE TABLE `trainer` (
  `trainerId` varchar(50) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `age` int(3) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `city` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `training` varchar(100) NOT NULL,
  `experience` int(2) NOT NULL,
  `file_address1` text NOT NULL,
  `specialized_field` varchar(255) NOT NULL,
  `salary` varchar(255) NOT NULL,
  `starting_date` date NOT NULL,
  `expiration_date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainer`
--

INSERT INTO `trainer` (`trainerId`, `fname`, `lname`, `sex`, `age`, `phone`, `city`, `email`, `training`, `experience`, `file_address1`, `specialized_field`, `salary`, `starting_date`, `expiration_date`, `status`, `view`) VALUES
('01', 'Aynalem', 'Belachew', 'Male', 29, '+215 924 433169', 'none', 'aynuka1@gmail.com', 'none', 6, '', '', '', '2017-04-17', '2017-05-06', 'contract_completed', 1),
('02', 'Ayele', 'Miga', 'Male', 31, '+251 913 623013', 'none', 'yonayene@gmail.com', 'Network+', 9, '', 'A+, Network+, MS Server, CCNA', '', '2017-04-17', '0000-00-00', 'unavailable', 1),
('03', 'Eyob', 'Asmelash', 'Male', 32, '+251 940 208371', 'none', 'eyonno6@gmail.com', 'CCNA', 8, '', 'CCNA, CCNP', '', '2017-04-17', '2017-04-16', 'contract_completed', 1),
('04', 'Michael', 'Mineweyelet', 'Male', 0, '+251 911 873021', 'none', 'gashmicky28@gmail.com', 'MCTS', 10, '', 'MCTS, CCNA', '', '2017-04-17', '2017-06-22', 'on_contract', 1),
('05', 'Tigabu', 'Dagne', 'Male', 33, '+251 911 103995', 'none', 'tgbdagne@gmail.com', 'CCNP', 0, '', 'CCNA, CCNP, CCDA, CCDP, MCSA, MCSE', '', '2017-04-17', '0000-00-00', 'available', 1),
('06', 'Bereket', 'Eshetu', 'Male', 35, '+251 911883469', 'Addis Ababa', 'bekethio@gmail.com', 'none', 0, '', 'Information System', '', '0000-00-00', '2017-04-29', 'on_contract', 1),
('07', 'Enkumicahel', 'Dereje', 'Male', 35, '+251 961443866,', 'Addis Ababa', 'enkumicahel@gmail.com', 'none', 0, '', 'Web Development', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('08', 'Meron', 'Tadesse', 'female', 35, '+251 913868059,', 'none', 'merontadesse08@gmail.com', 'none', 0, '', 'Information System', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('09', 'Fasica', 'Girma', 'female', 0, '+251 912 473158', 'none', 'sipara68@gmail.com', 'none', 0, '', 'CCNP', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('10', 'Tirsit', 'G/mariam', 'female', 35, '+251 921162684', 'none', 'galenegebremariam@gmail.com', 'none', 0, '', 'Networking', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('11', 'Zelalem', 'Dagnachew', 'Male', 35, '+251 929133894,', 'none', 'zeedam@gmail.com', 'CCNP', 0, '', 'CCNP, CCNA', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('12', 'Zemedkun', 'Merka', 'Male', 35, '+251 912088763', '', 'merka.zemedkun@gmail.com', 'none', 0, '', 'CCNA', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('13', '', 'Mohammed', 'female', 0, '+251 911088752', 'none', 'meooggmo@yahoo.com', 'none', 0, '', 'SAP, Microsoft Development', '', '0000-00-00', '2017-05-06', 'contract_completed', 1),
('14', 'Haregeweyin', 'Bekele', 'female', 0, '+251 916 453524', 'none', 'hareg23bekele@gmail.com', 'none', 0, '', 'Web Development, C++', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('15', 'Yonas', 'Tizazu', 'Male', 0, '+251 911 464207', 'none', 'tizyonas@gmail.com', 'none', 0, '', 'Application &amp; Web Security', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('16', 'Fetiha', 'Kedir', 'Male', 0, '+251 933 676855', 'none', '', 'none', 0, '', 'PHP', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('17', 'Tigist', 'Bete', 'female', 0, '+251 911 782817', 'none', 'tigi.bete@gmail.com', 'none', 0, '', 'Website dveelopment, C#', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('18', 'Hassen', 'Jibril', 'Male', 0, '+251 914 018 75', 'none', 'hjibril37@gmail.com', 'none', 0, '', 'Website development', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('19', 'Wonddwossen', 'Truneh', 'Male', 0, '', 'none', 'wtruneh@htomail.com', 'none', 0, '', 'Web Application Development', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('20', 'Letarik', 'Terefe', 'Male', 0, '+251 112 136608', 'none', 'sirletarik@gmail.com', 'none', 0, '', 'Software development', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('21', 'Yibeltal', 'Abebe', 'Male', 0, '+251 911 483311', 'Addis Ababa', 'yebeltal@uneca.org', 'none', 0, '', 'C#, SQL Server', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('22', 'Eyoel', 'Mitiku', 'Male', 0, '+251 911 844560', 'Addis Ababa', 'eyoelm@gmail.com', 'none', 0, '', 'Programming', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('23', 'Yesufie', 'Fenta', 'Male', 0, '+251 923 531946', 'Addis Ababa', '', 'none', 0, '', 'Website Development', '', '0000-00-00', '2017-05-02', 'contract_completed', 1),
('24', 'Liya', 'Tilahun', 'female', 0, '+251 921 275148', 'none', 'tilahunliyani@gmail.com', 'none', 0, '', 'Database', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('25', 'Kalkidan', 'Teshome', 'female', 0, '+251 912 902625', 'none', 'kalkidanteshome@gmail.com', 'none', 0, '', 'Computer Programming', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('26', 'Fikru', 'Yifter', 'Male', 0, '+251 939 920189', 'none', 'fikru6@yahoo.com', 'none', 0, '', 'Software Engineering', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('27', 'Frehiwot', 'Getinet', 'female', 0, '+251 911 163565', 'none', '', 'none', 0, '', 'Information Technology', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('28', 'Frezer', 'Damte', 'female', 0, '+251 910 622915', 'none', 'frezer49@yahoo.com', 'none', 0, '', 'Computer Engineering', '', '0000-00-00', '0000-00-00', 'on_contract', 0),
('29', 'Yimesgen', 'Birhanu', 'Male', 0, '+251 929 347499', 'none', 'yimesgenb@gmail.com', 'none', 0, '', 'Computer Science', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('30', 'Mesay', 'Wondosen', 'Male', 35, '+251 913821473', 'none', 'coffeeharar19@gmail.com', 'none', 0, '', 'Information Management', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('31', 'Yoseph', 'Mekasha', 'Male', 0, '', 'none', 'yoseph.mekasha@gmail.com', 'none', 0, '', 'Computer Science', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('32', 'Biruktawit', 'Sisay', 'female', 0, '+251 112 294282', 'none', 'biruksis42@gmail.com', 'none', 1, '', 'CCNA, CCNP', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('33', 'Amare', 'Muluken', 'Male', 0, '+251 943 886316', 'none', 'mulukenamare100@yahoo.com', 'none', 0, '', 'Computer Science', '', '0000-00-00', '0000-00-00', 'on_contract', 0),
('34', 'Kisanet', 'Eyob', 'female', 0, '+251 936 572572', 'none', 'kisanet2eyob@gmail.com', 'none', 0, '', 'Software Engineering', '', '0000-00-00', '2017-05-06', 'contract_completed', 1),
('35', 'Rebka', 'Demissie', 'female', 0, '+251 921 901624', 'none', 'rebeccademissie07@gmail.com', 'none', 0, '', 'Development', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('36', 'Rukiya', 'Nasir', 'female', 20, '+251 931 958684', 'none', '', 'none', 0, '', 'Database Administration', '', '0000-00-00', '0000-00-00', 'on_contract', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `trainer`
--
ALTER TABLE `trainer`
  ADD PRIMARY KEY (`trainerId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
