-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2017 at 10:59 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `date`, `title`, `message`) VALUES
(1, '0000-00-00', 'First notice', 'This is the first message posted on this wall.'),
(6, '2017-05-04', 'hjdsk', 'jdk'),
(7, '2017-05-04', 'jdsk', 'gjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsagjskakdjasdjhasjbasjdsakkdkadskdsa');

-- --------------------------------------------------------

--
-- Table structure for table `trainee`
--

CREATE TABLE `trainee` (
  `traineeId` varchar(50) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `age` int(3) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `company` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  `schedule_fee` varchar(255) NOT NULL,
  `starting_date` date NOT NULL,
  `expiration_date` date NOT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainee`
--

INSERT INTO `trainee` (`traineeId`, `fname`, `lname`, `sex`, `age`, `phone`, `email`, `city`, `company`, `class`, `status`, `schedule_fee`, `starting_date`, `expiration_date`, `view`) VALUES
('1', 'Hanna', 'Tamirat', 'female', 25, '+251 91254632', 'hanichotame@yahoo.com', 'Addis Ababa', 'St Paul Hospital', 'Eyob Asmelash: CCNA', 'contract_completed', 'CCNA: 4 months: 2000', '2017-04-01', '2017-04-17', 1),
('10', 'Kidane', 'Abebe', 'Male', 29, '00010', 'kidabbbbbbbbbbbb@yahoo.com', 'Other', 'SGT fgsy', 'Zemedkun Merka: none', 'contract_completed', 'Wordpress: 1 months: 1500', '2017-04-08', '2017-05-02', 1),
('2', 'Tizeta', 'Bekele', 'female', 26, '+251 913 09253429', 'tizebekele12@gmail.com', 'Addis Ababa', 'CBE', 'Tigabu Dagne: CCNP', 'on_contract', 'CCNP: 3 months: 6000', '2017-04-12', '2017-04-30', 0),
('3', 'Eyob', 'Abebaw', 'Male', 33, '+251 9109965325', 'eyobabebaw@yahoo.com', 'Addis Ababa', 'Eyob Consultancy', 'Tigabu Dagne: CCNP', 'on_contract', 'CCNP: 3 months: 6000', '2017-04-01', '2017-05-31', 1),
('4', 'Sirak', 'Adugna', 'Male', 33, '+251 913042317', 'sireAdugna12#@yahoo.com', 'Addis Ababa', 'INSA', 'Michael Mineweyelet: MCTS', 'contract_completed', 'MCTS: : 2000', '2017-04-01', '2017-05-02', 1),
('5', 'Tesfaye', 'Hailu', 'Male', 45, '+251 923456372', 'teafesh5hailu@hotmail.com', 'Addis Ababa', 'Yencomand Constructions', 'Michael Mineweyelet: MCTS', 'on_contract', 'MCTS: : 2000', '0000-00-00', '0000-00-00', 1),
('6', 'Biniam', 'Denberu', 'Male', 45, '+251 921654324', 'binidenb123@yahoo.com', 'Addis Ababa', 'MIE', 'Eyob Asmelash: CCNA', 'on_contract', 'CCNA: : 2000', '0000-00-00', '0000-00-00', 1),
('7', 'Kalkidan', 'Zegeye', 'female', 34, '+251 987345621', 'kalZeg@yahoo.com', 'Addis Ababa', 'CBE', 'Tigabu Dagne: CCNP', 'contract_completed', 'CCNP: 3 months: 6000', '2017-04-01', '2017-04-11', 0),
('8', 'Biruk', 'T/Berhan', 'Male', 26, '+251 91254632', 'birukt/b@gmail.com', 'Addis Ababa', 'Family Milk', 'Michael Mineweyelet: MCTS', 'contract_completed', 'MCTS: 3 months: 4000', '0000-00-00', '2017-05-03', 1),
('9', 'Habtamu', 'Werku', 'Male', 34, '+251 9346565652', 'habteshwerk@gmail.com', 'none', 'SGT International', 'none', 'contract_completed', 'none', '2017-04-27', '2017-05-05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trainer`
--

CREATE TABLE `trainer` (
  `trainerId` varchar(50) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `age` int(3) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `city` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `training` varchar(100) NOT NULL,
  `experience` int(2) NOT NULL,
  `specialized_field` varchar(255) NOT NULL,
  `specific_stay` varchar(10) NOT NULL,
  `salary` varchar(255) NOT NULL,
  `starting_date` date NOT NULL,
  `expiration_date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainer`
--

INSERT INTO `trainer` (`trainerId`, `fname`, `lname`, `sex`, `age`, `phone`, `city`, `email`, `training`, `experience`, `specialized_field`, `specific_stay`, `salary`, `starting_date`, `expiration_date`, `status`, `view`) VALUES
('01', 'Aynalem', 'Belachew', 'Male', 29, '+215 924 433169', 'none', 'aynuka1@gmail.com', 'none', 6, '', '3 months', '', '2017-04-17', '2017-05-06', 'contract_completed', 1),
('02', 'Ayele', 'Miga', 'Male', 31, '+251 913 623013', 'none', 'yonayene@gmail.com', 'Network+', 9, 'A+, Network+, MS Server, CCNA', '3 months', '', '2017-04-17', '0000-00-00', 'unavailable', 1),
('03', 'Eyob', 'Asmelash', 'Male', 32, '+251 940 208371', 'none', 'eyonno6@gmail.com', 'CCNA', 8, 'CCNA, CCNP', '3 months', '', '2017-04-17', '2017-04-16', 'contract_completed', 1),
('04', 'Michael', 'Mineweyelet', 'Male', 0, '+251 911 873021', 'none', 'gashmicky28@gmail.com', 'MCTS', 10, 'MCTS, CCNA', '3 months', '', '2017-04-17', '2017-06-22', 'on_contract', 1),
('05', 'Tigabu', 'Dagne', 'Male', 33, '+251 911 103995', 'none', 'tgbdagne@gmail.com', 'CCNP', 0, 'CCNA, CCNP, CCDA, CCDP, MCSA, MCSE', '3 months', '', '2017-04-17', '0000-00-00', 'available', 1),
('06', 'Bereket', 'Eshetu', 'Male', 35, '+251 911883469', 'Addis Ababa', 'bekethio@gmail.com', 'none', 0, 'Information System', '', '', '0000-00-00', '2017-04-29', 'on_contract', 1),
('07', 'Enkumicahel', 'Dereje', 'Male', 35, '+251 961443866,', 'Addis Ababa', 'enkumicahel@gmail.com', 'none', 0, 'Web Development', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('08', 'Meron', 'Tadesse', 'female', 35, '+251 913868059,', 'none', 'merontadesse08@gmail.com', 'none', 0, 'Information System', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('09', 'Fasica', 'Girma', 'female', 0, '+251 912 473158', 'none', 'sipara68@gmail.com', 'none', 0, 'CCNP', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('10', 'Tirsit', 'G/mariam', 'female', 35, '+251 921162684', 'none', 'galenegebremariam@gmail.com', 'none', 0, 'Networking', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('11', 'Zelalem', 'Dagnachew', 'Male', 35, '+251 929133894,', 'none', 'zeedam@gmail.com', 'CCNP', 0, 'CCNP, CCNA', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('12', 'Zemedkun', 'Merka', 'Male', 35, '+251 912088763', 'merka.zemedkun@gmail.com', '', 'none', 0, 'CCNA', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('13', 'Merima', 'Mohammed', 'female', 0, '+251 911088752', 'none', 'meooggmo@yahoo.com', 'none', 0, 'SAP, Microsoft Development', '', '', '0000-00-00', '2017-05-06', 'contract_completed', 1),
('14', 'Haregeweyin', 'Bekele', 'female', 0, '+251 916 453524', 'none', 'hareg23bekele@gmail.com', 'none', 0, 'Web Development, C++', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('15', 'Yonas', 'Tizazu', 'Male', 0, '+251 911 464207', 'none', 'tizyonas@gmail.com', 'none', 0, 'Application &amp; Web Security', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('16', 'Fetiha', 'Kedir', 'Male', 0, '+251 933 676855', 'none', '', 'none', 0, 'PHP', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('17', 'Tigist', 'Bete', 'female', 0, '+251 911 782817', 'none', 'tigi.bete@gmail.com', 'none', 0, 'Website dveelopment, C#', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('18', 'Hassen', 'Jibril', 'Male', 0, '+251 914 018 75', 'none', 'hjibril37@gmail.com', 'none', 0, 'Website development', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('19', 'Wonddwossen', 'Truneh', 'Male', 0, '', 'none', 'wtruneh@htomail.com', 'none', 0, 'Web Application Development', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('20', 'Letarik', 'Terefe', 'Male', 0, '+251 112 136608', 'none', 'sirletarik@gmail.com', 'none', 0, 'Software development', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('21', 'Yibeltal', 'Abebe', 'Male', 0, '+251 911 483311', 'Addis Ababa', 'yebeltal@uneca.org', 'none', 0, 'C#, SQL Server', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('22', 'Eyoel', 'Mitiku', 'Male', 0, '+251 911 844560', 'Addis Ababa', 'eyoelm@gmail.com', 'none', 0, 'Programming', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('23', 'Yesufie', 'Fenta', 'Male', 0, '+251 923 531946', 'Addis Ababa', '', 'none', 0, 'Website Development', '', '', '0000-00-00', '2017-05-02', 'contract_completed', 1),
('24', 'Liya', 'Tilahun', 'female', 0, '+251 921 275148', 'none', 'tilahunliyani@gmail.com', 'none', 0, 'Database', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('25', 'Kalkidan', 'Teshome', 'female', 0, '+251 912 902625', 'none', 'kalkidanteshome@gmail.com', 'none', 0, 'Computer Programming', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('26', 'Fikru', 'Yifter', 'Male', 0, '+251 939 920189', 'none', 'fikru6@yahoo.com', 'none', 0, 'Software Engineering', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('27', 'Frehiwot', 'Getinet', 'female', 0, '+251 911 163565', 'none', '', 'none', 0, 'Information Technology', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('28', 'Frezer', 'Damte', 'female', 0, '+251 910 622915', 'none', 'frezer49@yahoo.com', 'none', 0, 'Computer Engineering', '', '', '0000-00-00', '0000-00-00', 'on_contract', 0),
('29', 'Yimesgen', 'Birhanu', 'Male', 0, '+251 929 347499', 'none', 'yimesgenb@gmail.com', 'none', 0, 'Computer Science', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('30', 'Mesay', 'Wondosen', 'Male', 35, '+251 913821473', 'none', 'coffeeharar19@gmail.com', 'none', 0, 'Information Management', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('31', 'Yoseph', 'Mekasha', 'Male', 0, '', 'none', 'yoseph.mekasha@gmail.com', 'none', 0, 'Computer Science', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('32', 'Biruktawit', 'Sisay', 'female', 0, '+251 112 294282', 'none', 'biruksis42@gmail.com', 'none', 1, 'CCNA, CCNP', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('33', 'Amare', 'Muluken', 'Male', 0, '+251 943 886316', 'none', 'mulukenamare100@yahoo.com', 'none', 0, 'Computer Science', '', '', '0000-00-00', '0000-00-00', 'on_contract', 0),
('34', 'Kisanet', 'Eyob', 'female', 0, '+251 936 572572', 'none', 'kisanet2eyob@gmail.com', 'none', 0, 'Software Engineering', '', '', '0000-00-00', '2017-05-06', 'contract_completed', 1),
('35', 'Rebka', 'Demissie', 'female', 0, '+251 921 901624', 'none', 'rebeccademissie07@gmail.com', 'none', 0, 'Development', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('36', 'Rukiya', 'Nasir', 'female', 20, '+251 931 958684', 'none', '', 'none', 0, 'Database Administration', '', '', '0000-00-00', '0000-00-00', 'on_contract', 1),
('37', 'Habtamu', 'Olika', 'Male', 45, '00000', 'Other', 'habteshwelik1111111@gmail.com', 'Ms Exchange', 1, 'bsfs', '2 days', '1', '2017-04-29', '2017-05-06', 'contract_completed', 0);

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `training_id` int(11) NOT NULL,
  `training` varchar(100) NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `specify_schedule` varchar(255) NOT NULL,
  `fee` double NOT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`training_id`, `training`, `vendor`, `specify_schedule`, `fee`, `view`) VALUES
(1, 'Ms Exchange', 'Microsoft', '3 months', 6000, 1),
(2, 'SharePoint', 'Microsoft', '3 months', 2000, 1),
(3, 'SQL Server', 'Microsoft', '3 months', 2000, 1),
(4, 'MS Servers', 'Microsoft', '3 months', 2000, 1),
(5, 'Ms Office 365', 'Microsoft', '2 months', 4000, 1),
(6, 'CCNA', 'Cisco', '4 months', 2000, 1),
(7, 'CCNP', 'Cisco', '3 months', 6000, 1),
(8, 'HCNA', 'Huawei', '3 months', 1000, 1),
(9, 'HCNP', 'Huawei', '6 months', 2500, 1),
(10, 'A+', 'Comp TIA', '2 months', 2000, 1),
(11, 'Network+', 'Comp TIA', '4 months', 4000, 1),
(12, 'Security+', 'Comp TIA', '3 months', 2500, 1),
(13, 'PMP Training', 'PMI', '2 months', 2600, 1),
(14, 'Wordpress', 'Open Source', '1 months', 1500, 1),
(15, 'Joomla', 'Open Source', '1 month', 1000, 1),
(16, 'Drupal', 'Open Source', '2 months', 2500, 1),
(17, 'Adobe Photoshop', 'Adobe', '2 months', 4000, 1),
(18, 'Adobe Illustrator', 'Adobe', '2 months', 3500, 1),
(19, 'CCDP', 'Cisco', '4 months', 5500, 1),
(20, 'CCDA', 'Cisco', '6 months', 8000, 1),
(21, 'MCSE Private Cloud', 'Microsoft', '3 months', 2500, 1),
(22, 'MCSA', 'Microsoft', '2 months', 1500, 1),
(23, 'MCTS', 'Microsoft', '3 months', 4000, 1),
(24, 'SQL Server', '', '3 months', 2000, 1),
(25, 'VMware', '', '1 month', 1200, 1),
(26, 'Citrix XenServer', '', '3 months', 2500, 1),
(27, 'Citrix XenApp', '', '4 months', 3000, 1),
(28, 'Citrix XenDesktop', '', '1 month', 2000, 1),
(456, 'dfghjk', 'fdghjk', 'dfghj', 0, 1),
(567, 'c++`', 'fghj', '3456', 2332, 1),
(3546, 'DFGH', 'DFGH', 'FDGH', 0, 0),
(4356, 'dfghjkl', 'fdghjk', 'fdghj', 0, 0),
(4567, 'sgdsf', 'gfhj', 'gfhj', 0, 1),
(563265, 'Java', 'Java', '55', 567, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`, `role`, `status`) VALUES
(1, 'Administrator', 'Admin', 'c4ca4238a0b923820dcc509a6f75849b', 'administrator', 1),
(2, 'Registrar', 'reg', 'c81e728d9d4c2f636f067f89cc14862c', 'registrar', 1),
(3, 'Controller', 'control', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 'controller', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainee`
--
ALTER TABLE `trainee`
  ADD PRIMARY KEY (`traineeId`);

--
-- Indexes for table `trainer`
--
ALTER TABLE `trainer`
  ADD PRIMARY KEY (`trainerId`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`training_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
